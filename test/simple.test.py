import os
import unittest

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import TimerAction
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

import launch_testing
import launch_testing.markers

@launch_testing.markers.keep_alive
def generate_test_description():
    launch_description = LaunchDescription([
        Node(
            package='orocos_examples',
            executable=['simple_ros_rtt-{OROCOS_TARGET}'.format_map(os.environ)],
            name='simple',
            arguments=['--name', 'simple'],
            parameters=[{'connect_to_topics': True}],
            output='screen',
            emulate_tty=True
        ),

        TimerAction(period=10.0, actions=[]),

        Node(
            package='orocos_examples',
            executable='simple_rostest',
            output='screen'
        ),

        launch_testing.actions.ReadyToTest()
    ])

    return launch_description, {'processes_to_test': ['simple_ros_rtt-{OROCOS_TARGET}'.format_map(os.environ), 'simple_rostest']}

class TestLoggingOutputFormat(unittest.TestCase):

    def test_logging_output(self, proc_info, proc_output, processes_to_test):
        for process_name in processes_to_test:
            proc_info.assertWaitForShutdown(process=process_name, timeout=20.0)
