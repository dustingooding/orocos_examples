/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include <rtt/os/main.h>
#include <ocl/DeploymentComponent.hpp>
#include <rtt/extras/SlaveActivity.hpp>

int ORO_main(int argc, char* argv[])
{
    std::string component_name = "foo";
    OCL::DeploymentComponent deployer;

    if (not deployer.import("orocos_examples"))
    {
        std::cerr << "Orocos package orocos_examples could not be imported." << std::endl;
        return 1;
    }

    if (not deployer.loadComponent(component_name, "orocos_examples::Empty"))
    {
        std::cerr << "Failed to load orocos_examples::Empty component named [foo]." << std::endl;
        return 1;
    }

    RTT::TaskContext* empty_component = deployer.getPeer(component_name);

    if (not empty_component)
    {
        std::cerr << "Failed to get pointer to [foo]." << std::endl;
        return 1;
    }

    // THIS IS ONLY REQUIRED FOR DEMONSTRATION PURPOSES.
    // WITHOUT ESTABLISHING "empty_component" WITH A SLAVE ACVTIVIY,
    // YOU CANNOT EXTERNALLY CALL empty_component->update()
    empty_component->setActivity(new RTT::extras::SlaveActivity());

    if (not empty_component->configure())
    {
        std::cerr << "Failed to configure [foo]." << std::endl;
        return 1;
    }

    if (not empty_component->start())
    {
        std::cerr << "Failed to start [foo]." << std::endl;
        return 1;
    }

    if (not empty_component->update())
    {
        std::cerr << "Failed to update [foo]." << std::endl;
        return 1;
    }

    empty_component->error();

    if (not empty_component->update())
    {
        std::cerr << "Failed to update [foo]." << std::endl;
        return 1;
    }

    if (not empty_component->update())
    {
        std::cerr << "Failed to update [foo]." << std::endl;
        return 1;
    }

    if (not empty_component->stop())
    {
        std::cerr << "Failed to stop [foo]." << std::endl;
        return 1;
    }

    if (not empty_component->cleanup())
    {
        std::cerr << "Failed to cleanup [foo]." << std::endl;
        return 1;
    }

    if (not deployer.kickOutAll())
    {
        std::cerr << "Failed to unload [foo]." << std::endl;
        return 1;
    }

    deployer.shutdownDeployment();

    return 0;
}
