/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include "orocos_examples/simple_ros.hpp"

namespace orocos_examples
{
SimpleRos::SimpleRos(const std::string& name)
  : TaskContext(name)
  , connect_to_topics_(false)
  , property_(0)
  , input_port_value_(0)
  , event_port_value_(0)
  , event_port_cb_value_(0)
  , op_callback_value_(0)
  , op_caller_("simple_operation_caller")
{
    std::cout << "SimpleRos [" << getName() << "]: constructed" << std::endl;

    this->addProperty("connect_to_topics", connect_to_topics_).doc("ROS Topic connection guard");
    this->addProperty("property", property_).doc("Simple Property");
    this->addProperty("input_port_value", input_port_value_).doc("Latest value processed on Simple Input Port");
    this->addProperty("event_port_value", event_port_value_).doc("Latest value processed on Simple Event Port");
    this->addProperty("event_port_cb_value", event_port_cb_value_).doc("Latest value processed on Event Port with Callback");

    this->ports()->addPort("input_port", input_port_).doc("Simple Input Port");
    this->ports()->addPort("output_port", output_port_).doc("Simple Output Port");

    this->ports()->addEventPort("event_port", event_port_).doc("Simple Event Port with ROS Message");
    this->ports()->addEventPort("event_port_cb", event_port_cb_, boost::bind(&SimpleRos::eventPortCallback, this)).doc("ROS Message Event Port with Callback.");

    this->provides("simple_service")
        ->addOperation("simple_operation", &SimpleRos::op_callback, this, RTT::ClientThread)
        .arg("request", "An integer input.")
        .arg("response", "An bool output.")
        .doc("Simple Operation with ROS Messages");
    this->requires()->addOperationCaller(op_caller_);
}

SimpleRos::~SimpleRos()
{
    std::cout << "SimpleRos [" << getName() << "]: destructed" << std::endl;
}

bool SimpleRos::configureHook()
{
    std::cout << "SimpleRos [" << getName() << "]: configured" << std::endl;

    bool success = true;

    boost::shared_ptr<rtt_ros2_params::RosParam> rosparam = this->getProvider<rtt_ros2_params::RosParam>("rosparam");
    if (rosparam and rosparam->ready())
    {
        if (not rosparam->loadProperty("connect_to_topics", "connect_to_topics"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [connect_to_topics]" << std::endl;
        }
        if (not rosparam->storeProperty("property", "property"))  // set() here to initialize rosparam value
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
        if (not rosparam->storeProperty("input_port_value", "input_port_value"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
        if (not rosparam->storeProperty("event_port_value", "event_port_value"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
        if (not rosparam->storeProperty("event_port_cb_value", "event_port_cb_value"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
    }

    // There is no programmatic way to see if ROS topic streaming is available.
    // If you want to guard this, use a Property set from outside.
    if (connect_to_topics_)
    {
        if (not input_port_.createStream(rtt_ros2_topics::topic("/" + getName() + "/input_port")))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to connect to ROS topic [~/input_port]" << std::endl;
            success = false;
        }
        if (not output_port_.createStream(rtt_ros2_topics::topic("/" + getName() + "/output_port")))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to connect to ROS topic [~/output_port]" << std::endl;
            success = false;
        }
        if (not event_port_.createStream(rtt_ros2_topics::topic("/" + getName() + "/event_port")))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to connect to ROS topic [~/event_port]" << std::endl;
            success = false;
        }
        if (not event_port_cb_.createStream(rtt_ros2_topics::topic("/" + getName() + "/event_port_cb")))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to connect to ROS topic [~/event_port_cb]" << std::endl;
            success = false;
        }
    }

    boost::shared_ptr<rtt_ros2_services::RosService> rosservice = this->getProvider<rtt_ros2_services::RosService>("rosservice");
    if (rosservice and rosservice->ready())
    {
        if (not rosservice->connect("simple_service.simple_operation", "/" + getName() + "/simple_operation", "oe_msgs/srv/Operation"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to connect to ROS service [~/simple_operation]" << std::endl;
            success = false;
        }
        if (not rosservice->connect("simple_operation_caller", "/external/operation", "oe_msgs/srv/Operation"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to connect to ROS service [~/simple_operation_caller]" << std::endl;
            success = false;
        }
    }

    return success;
}

bool SimpleRos::startHook()
{
    std::cout << "SimpleRos [" << getName() << "]: started" << std::endl;
    return true;
}

void SimpleRos::updateHook()
{
    std::cout << "SimpleRos [" << getName() << "]: updated" << std::endl;

    // Process Inputs
    std_msgs::msg::Int32 input_temp;
    if (input_port_.read(input_temp) == RTT::FlowStatus::NewData)
    {
        input_port_value_ = input_temp.data;
    }

    std_msgs::msg::Int32 event_temp;
    if (event_port_.read(event_temp) == RTT::FlowStatus::NewData)
    {
        event_port_value_ = event_temp.data;
    }

    // Do Something
    std_msgs::msg::Int32 calculated_sum;
    calculated_sum.data = property_ + input_port_value_ + event_port_value_ + event_port_cb_value_;

    if (op_caller_.ready())
    {
        oe_msgs::srv::Operation::Request request;
        oe_msgs::srv::Operation::Response response;
        request.value.data = calculated_sum.data;
        op_caller_(request, response);
        if (response.value.data)
        {
            std::cout << "SimpleRos [" << getName() << "]: simple_operation_caller called successfully with argument = " << request.value.data << std::endl;
        }
        else
        {
            std::cerr << "SimpleRos [" << getName() << "]: simple_operation_caller failed with argument = " << request.value.data << std::endl;
        }
    }
    else
    {
        std::cerr << "SimpleRos [" << getName() << "]: simple_operation_caller not ready to be called" << std::endl;
    }

    // Process Outputs
    if (not output_port_.write(calculated_sum) == RTT::WriteStatus::WriteSuccess)
    {
        std::cerr << "SimpleRos [" << getName() << "]: output_port failed to write" << std::endl;
    }

    boost::shared_ptr<rtt_ros2_params::RosParam> rosparam = this->getProvider<rtt_ros2_params::RosParam>("rosparam");
    if (rosparam and rosparam->ready())
    {
        if (not rosparam->loadProperty("property", "property"))  // get() here to grab latest value from rosparam
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
        if (not rosparam->storeProperty("input_port_value", "input_port_value"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
        if (not rosparam->storeProperty("event_port_value", "event_port_value"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
        if (not rosparam->storeProperty("event_port_cb_value", "event_port_cb_value"))
        {
            std::cout << "SimpleRos [" << getName() << "]: failed to update rosparam [property]" << std::endl;
        }
    }
}

void SimpleRos::stopHook()
{
    std::cout << "SimpleRos [" << getName() << "]: stopped" << std::endl;
}

void SimpleRos::cleanupHook()
{
    std::cout << "SimpleRos [" << getName() << "]: cleaned" << std::endl;
}

void SimpleRos::eventPortCallback()
{
    std_msgs::msg::Int32 event_cb_temp;
    event_port_cb_.readNewest(event_cb_temp);
    event_port_cb_value_ = event_cb_temp.data;
}

void SimpleRos::op_callback(oe_msgs::srv::Operation::Request& request, oe_msgs::srv::Operation::Response& response)
{
    std::cout << "SimpleRos [" << getName() << "]: simple_operation executed successfully with argument = " << request.value.data << std::endl;

    response.value.data = true;
}

}  // namespace orocos_examples

ORO_CREATE_COMPONENT(orocos_examples::SimpleRos)
